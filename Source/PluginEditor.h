#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "TwoValueSliderAttachment.h"
#include "MyTheme.h"
//#include "MyTheme.h"

//==============================================================================
class PluginEditor  : public juce::AudioProcessorEditor
{
public:
    explicit PluginEditor (Bubbbleizer& p, AudioProcessorValueTreeState& vts);
    ~PluginEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:

    Bubbbleizer& processorRef;
    AudioProcessorValueTreeState& valueTreeState;
    MyLookAndFeel mylaf;

    std::unique_ptr<juce::Slider> BNSlider;
    std::unique_ptr<juce::Slider> MRSlider; //max and min radioud
    std::unique_ptr<juce::Slider> ERSlider;
    std::unique_ptr<juce::Slider> MDSlider; //max and min depth
    std::unique_ptr<juce::Slider> EDSlider;
    std::unique_ptr<juce::Slider> RFSlider;
    std::unique_ptr<juce::Slider> RCSlider;
    std::unique_ptr<juce::Slider> ARSlider;
    std::unique_ptr<juce::Slider> GASlider;
    std::unique_ptr<juce::ToggleButton> IIButton;

    juce::Label BNLabel;
    juce::Label MRLabel; //max and min radius
    juce::Label ERLabel;
    juce::Label MDLabel; //max and min depth
    juce::Label EDLabel;
    juce::Label RFLabel;
    juce::Label RCLabel;
    juce::Label ARLabel;
    juce::Label GALabel;

    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> BNAttachment;
    std::unique_ptr<                            TwoValueSliderAttachment> MRAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> ERAttachment;
    std::unique_ptr<                            TwoValueSliderAttachment> MDAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> EDAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> RFAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> RCAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> ARAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> GAAttachment;
    std::unique_ptr<juce::AudioProcessorValueTreeState::ButtonAttachment> IIAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginEditor)
};
