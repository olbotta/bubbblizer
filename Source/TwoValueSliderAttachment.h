#include <JuceHeader.h>
#include "CustomParameterAttachment.h"
using namespace juce;
//credits: https://forum.juce.com/t/apvts-without-attachments-with-respect-to-a-twovalueslider/36591/11


class TwoValueSliderAttachment {
public:


    TwoValueSliderAttachment(AudioProcessorValueTreeState& vts, const String minParamID, const String maxParamID, Slider& slider)
    : minValue (), maxValue () {

        auto minParam = vts.getParameter(minParamID);
        auto maxParam = vts.getParameter(maxParamID);

        // Expect minParam == maxParam
        jassert(minParam->getNormalisableRange().start == maxParam->getNormalisableRange().start
                && minParam->getNormalisableRange().end
                == maxParam->getNormalisableRange().end);

        auto minDefault = minParam->convertFrom0to1(minParam->getDefaultValue());
        auto maxDefault = maxParam->convertFrom0to1(maxParam->getDefaultValue());

        slider.setRange(minParam->getNormalisableRange().start, maxParam->getNormalisableRange().end);
        slider.setMinAndMaxValues(minDefault, maxDefault);

        slider.onDragStart = [&]
        {
            lastDraggedThumb = slider.getThumbBeingDragged();
            if (lastDraggedThumb == 1) minValue.beginGesture();
            else if (lastDraggedThumb == 2) maxValue.beginGesture();
            slider.updateText();
        };
        slider.onDragEnd = [&]
        {
            if (lastDraggedThumb == 1) minValue.endGesture();
            else if (lastDraggedThumb == 2) maxValue.endGesture();
            lastDraggedThumb = 0;
            slider.updateText();
        };

        slider.onValueChange = [&]
        {
            if (lastDraggedThumb == 1) minValue.setValue(slider.getMinValue());
            else if (lastDraggedThumb == 2) maxValue.setValue(slider.getMaxValue());
            slider.updateText();
        };

        slider.textFromValueFunction = [&] (double input)
        {
            juce::ignoreUnused(input);
            String output = String(slider.getMinValue(), 2);
            output += " - ";
            output += String(slider.getMaxValue(), 2);
            return output;
        };


        minValue.onParameterChangedAsync = [&] { slider.setMinValue (minValue.getValue()); };
        maxValue.onParameterChangedAsync = [&] { slider.setMaxValue (maxValue.getValue()); };

        minValue.attachToParameter (minParam);
        maxValue.attachToParameter (maxParam);

    }

private:

    ciaone::ParameterAttachment<float> minValue;
    ciaone::ParameterAttachment<float> maxValue;
    int lastDraggedThumb = 0;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TwoValueSliderAttachment)
};
