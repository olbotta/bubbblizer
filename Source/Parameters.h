//TODO: far in modo che si inizializzio i vlaori del VST in modo che funzioni appena aperto
#pragma once
#include <JuceHeader.h>

#define MIN_RADIUS      0.00015
#define MAX_RADIUS      0.150
#define MAX_EXP        10.0
#define MAX_RISE        3.0
#define MAX_RATE   100000.0

#define N_CONTROLS 10 //number of displayed controls

#define SMOOTHING_TIME 0.02f //for smoothed values

// Parameters IDs
#define NAME_BN  "bubblesNumber"
#define NAME_MIR "minRadius"
#define NAME_MAR "maxRadius"
#define NAME_ER  "expRadius"    //gamma factor for the radius assignment
#define NAME_MID "minDepth"
#define NAME_MAD "maxDepth"     //https://www.youtube.com/watch?v=vLJz2TLt5bI
#define NAME_ED  "expDepth"     //gamma factor for the depth assignment
#define NAME_RF  "riseFactor"   //amount of blooping for the bubble population
#define NAME_RC  "riseCutoff"   //Bubbles deeper than this threshold do not rise in frequency
#define NAME_AR  "averageRate"  //of bubble generation per second
#define NAME_II  "ignoreInput"  //if on ignores inpur
#define NAME_GA  "gain"

// default values (copied from SDT)
#define DEFAULT_BN 0.0f
#define DEFAULT_MIR 0.0f
#define DEFAULT_MAR 0.01f
#define DEFAULT_ER 3.0f
#define DEFAULT_MID 0.0f
#define DEFAULT_MAD 0.3f
#define DEFAULT_ED 3.0f
#define DEFAULT_RF 0.1f
#define DEFAULT_RC 0.9f
#define DEFAULT_AR 0.0f
#define DEFAULT_GA 1.0f
#define DEFAULT_II false


namespace Parameters
{
	static AudioProcessorValueTreeState::ParameterLayout createParameterLayout()
	{
        std::vector<std::unique_ptr<RangedAudioParameter>> params;
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_BN,  "Bubbbles number", 0.0f,  200.0f, DEFAULT_BN));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_GA,  "Gain",            0.0f,    1.0f, DEFAULT_GA));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_ER,  "Radius gamma",    0.0f, MAX_EXP, DEFAULT_ER));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_MID, "Maximum depth",   0.0f,    1.0f, DEFAULT_MID));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_MAD, "Minimum depth",   0.0f,    1.0f, DEFAULT_MAD));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_ED,  "Depth gamma",     0.0f, MAX_EXP, DEFAULT_ED));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_RF,  "Bloop amount",    0.0f,MAX_RISE, DEFAULT_RF));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_RC,  "Min bloop depth", 0.0f,    1.0f, DEFAULT_RC));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_MIR, "Minimum radius",
                                                               NormalisableRange<float>(MIN_RADIUS*100,MAX_RADIUS*100/2,0.01f), DEFAULT_MIR*100));
                //*100 (per ottenere centimetri) / 2 (per diminuire il range)
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_MAR, "Maximum radius",
                                                               NormalisableRange<float>(MIN_RADIUS*100,MAX_RADIUS*100/2,0.01f), DEFAULT_MAR*100));
        params.push_back(std::make_unique<AudioParameterFloat>(NAME_AR, "Generation rate",
                                                               NormalisableRange<float>(0.0f,MAX_RATE,1.0f,0.4f), DEFAULT_AR));
        params.push_back(std::make_unique<AudioParameterBool>(NAME_II, "Ignore input",DEFAULT_II));

//        params.push_back(std::make_unique<AudioParameterFloat>(NAME_MAR, "maximum radius",  MIN_RADIUS, MAX_RADIUS, DEFAULT_MR));
//        params.push_back(std::make_unique<AudioParameterFloat>(NAME_MIR, "minimum radius",  MIN_RADIUS, MAX_RADIUS, DEFAULT_MR));
//        params.push_back(std::make_unique<AudioParameterFloat>(NAME_AR,  "average gen rate",  0.0f, MAX_RATE/100, DEFAULT_AR));

		return {params.begin(), params.end()};
	}
}
