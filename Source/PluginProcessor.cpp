#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Parameters.h"

//==============================================================================
Bubbbleizer::Bubbbleizer()
     : AudioProcessor (BusesProperties().withInput  ("Input",  juce::AudioChannelSet::stereo(), true).withOutput ("Output", juce::AudioChannelSet::stereo(), true)),
       parameters(*this, nullptr, "BubbblerParameters", Parameters::createParameterLayout())
{
    fluidflow = SDTFluidFlow_new (100);
    parameters.addParameterListener(NAME_BN,  this);
    parameters.addParameterListener(NAME_MIR, this);
    parameters.addParameterListener(NAME_MAR, this);
    parameters.addParameterListener(NAME_ER,  this);
    parameters.addParameterListener(NAME_MID, this);
    parameters.addParameterListener(NAME_MAD, this);
    parameters.addParameterListener(NAME_ED,  this);
    parameters.addParameterListener(NAME_RF,  this);
    parameters.addParameterListener(NAME_RC,  this);
    parameters.addParameterListener(NAME_AR,  this);
    parameters.addParameterListener(NAME_GA,  this);
    parameters.addParameterListener(NAME_II,  this);
}

Bubbbleizer::~Bubbbleizer()
{
    SDTFluidFlow_free(fluidflow);
}

//==============================================================================
void Bubbbleizer::prepareToPlay (double sampleRate, int )
{
    SDT_setSampleRate(sampleRate);
    gain.reset(sampleRate, SMOOTHING_TIME);
    auto* p = parameters.getParameter (NAME_AR);
    parameterChanged(NAME_AR , p->convertFrom0to1 (p->getValue ()));
}

void Bubbbleizer::releaseResources()
{
}

void Bubbbleizer::processBlock (juce::AudioBuffer<float>& buffer,
                                              juce::MidiBuffer& midiMessages)
{
    juce::ignoreUnused (midiMessages);
    auto bufferData = buffer.getArrayOfWritePointers();
    auto numCh = buffer.getNumChannels();
    auto numSp = buffer.getNumSamples ();

    double bubbles;

    for (auto smp = 0; smp < numSp; ++smp)
    {
        bubbles = SDTFluidFlow_dsp (fluidflow);
        for (auto ch = 0; ch < numCh; ++ch)
            bufferData[ch][smp] = ignoreInput ? bubbles : bubbles*bufferData[ch][smp];
    }

    gain.getNextValue ();
    gain.applyGain (buffer,numSp);

}

//==============================================================================
//bool Bubbbleizer::isBusesLayoutSupported (const BusesLayout& layouts) const
//{

//    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
//     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
//        return false;

//    // This checks if the input layout matches the output layout
//   #if ! JucePlugin_IsSynth
//    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
//        return false;
//   #endif

//    return true;
//}

juce::AudioProcessorEditor* Bubbbleizer::createEditor()
{
    return new PluginEditor (*this, parameters);
}

//==============================================================================
void Bubbbleizer::getStateInformation (juce::MemoryBlock& destData)
{
    auto state = parameters.copyState();
    std::unique_ptr<XmlElement> xml(state.createXml());
    copyXmlToBinary(*xml, destData);
}

void Bubbbleizer::setStateInformation (const void* data, int sizeInBytes)
{
    std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
    if (xmlState.get() != nullptr)
        if (xmlState->hasTagName(parameters.state.getType()))
            parameters.replaceState(ValueTree::fromXml(*xmlState));
}

void Bubbbleizer::parameterChanged(const String& paramID, float newValue)
{
         if (paramID == NAME_BN ) SDTFluidFlow_setNBubbles   (fluidflow,roundToInt (newValue));
    else if (paramID == NAME_MIR) SDTFluidFlow_setMinRadius  (fluidflow,(double)newValue/100);
    else if (paramID == NAME_MAR) SDTFluidFlow_setMaxRadius  (fluidflow,(double)newValue/100);
    else if (paramID == NAME_ER)  SDTFluidFlow_setExpRadius  (fluidflow,(double)newValue);
    else if (paramID == NAME_MID) SDTFluidFlow_setMinDepth   (fluidflow,(double)newValue);
    else if (paramID == NAME_MAD) SDTFluidFlow_setMaxDepth   (fluidflow,(double)newValue);
    else if (paramID == NAME_ED)  SDTFluidFlow_setExpDepth   (fluidflow,(double)newValue);
    else if (paramID == NAME_RF ) SDTFluidFlow_setRiseFactor (fluidflow,(double)newValue);
    else if (paramID == NAME_RC ) SDTFluidFlow_setRiseCutoff (fluidflow,(double)newValue);
    else if (paramID == NAME_AR ) SDTFluidFlow_setAvgRate    (fluidflow,(double)newValue);
    else if (paramID == NAME_GA ) gain.setTargetValue (newValue);
    else if (paramID == NAME_II ) ignoreInput = roundToInt(newValue)%2;

}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Bubbbleizer();
}
