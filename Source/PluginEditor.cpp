#include "Parameters.h"
#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
PluginEditor::PluginEditor (Bubbbleizer& p, AudioProcessorValueTreeState& vts)
    : AudioProcessorEditor (&p), processorRef (p), valueTreeState (vts)
{
    BNSlider.reset (new juce::Slider ("BNSlider"));
    BNSlider->setSliderStyle (juce::Slider::LinearHorizontal);
    BNSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    BNLabel.setText (valueTreeState.getParameter (NAME_BN)->getName (20), juce::dontSendNotification);
    BNLabel.attachToComponent (BNSlider.get(), true);
    addAndMakeVisible (BNLabel);
    addAndMakeVisible (BNSlider.get());

    MRSlider.reset (new juce::Slider ("MRSlider"));
    MRSlider->setSliderStyle (juce::Slider::TwoValueHorizontal);
    MRSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    MRSlider->setTextValueSuffix (" cm");
    MRLabel.setText ("bubbbles radius", juce::dontSendNotification);
    MRLabel.attachToComponent (MRSlider.get(), true);
    addAndMakeVisible (MRLabel);
    addAndMakeVisible (MRSlider.get());

    ERSlider.reset (new juce::Slider ("EXRSlider"));
    ERSlider->setSliderStyle (juce::Slider::LinearHorizontal);
    ERSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    ERLabel.setText (valueTreeState.getParameter (NAME_ER)->getName (20), juce::dontSendNotification);
    ERLabel.attachToComponent (ERSlider.get(), true);
    addAndMakeVisible (ERLabel);
    addAndMakeVisible (ERSlider.get());

    MDSlider.reset (new juce::Slider ("MDSlider"));
    MDSlider->setSliderStyle (juce::Slider::TwoValueHorizontal);
    MDSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    MDLabel.setText ("bubbbles distance", juce::dontSendNotification);
    MDLabel.attachToComponent (MDSlider.get(), true);
    addAndMakeVisible (MDLabel);
    addAndMakeVisible (MDSlider.get());

    EDSlider.reset (new juce::Slider ("EDSlider"));
    EDSlider->setSliderStyle (juce::Slider::LinearHorizontal);
    EDSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    EDLabel.setText (valueTreeState.getParameter (NAME_ED)->getName (20), juce::dontSendNotification);
    EDLabel.attachToComponent (EDSlider.get(), true);
    addAndMakeVisible (EDLabel);
    addAndMakeVisible (EDSlider.get());

    RFSlider.reset (new juce::Slider ("RFRSlider"));
    RFSlider->setSliderStyle (juce::Slider::LinearHorizontal);
    RFSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    RFLabel.setText (valueTreeState.getParameter (NAME_RF)->getName (20), juce::dontSendNotification);
    RFLabel.attachToComponent (RFSlider.get(), true);
    addAndMakeVisible (RFLabel);
    addAndMakeVisible (RFSlider.get());

    RCSlider.reset (new juce::Slider ("RCSlider"));
    RCSlider->setSliderStyle (juce::Slider::LinearHorizontal);
    RCSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    RCLabel.setText (valueTreeState.getParameter (NAME_RC)->getName (20), juce::dontSendNotification);
    RCLabel.attachToComponent (RCSlider.get(), true);
    addAndMakeVisible (RCLabel);
    addAndMakeVisible (RCSlider.get());

    ARSlider.reset (new juce::Slider ("ARSlider"));
    ARSlider->setSliderStyle (juce::Slider::LinearHorizontal);
    ARSlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    ARSlider->setTextValueSuffix (" #/s");
    ARLabel.setText (valueTreeState.getParameter (NAME_AR)->getName (20), juce::dontSendNotification);
    ARLabel.attachToComponent (ARSlider.get(), true);
    addAndMakeVisible (ARLabel);
    addAndMakeVisible (ARSlider.get());

    GASlider.reset (new juce::Slider ("GASlider"));
    GASlider->setSliderStyle (juce::Slider::LinearHorizontal);
    GASlider->setTextBoxStyle (juce::Slider::TextBoxRight, false, 100, 20);
    GALabel.setText (valueTreeState.getParameter (NAME_GA)->getName (20), juce::dontSendNotification);
    GALabel.attachToComponent (GASlider.get(), true);
    addAndMakeVisible (GALabel);
    addAndMakeVisible (GASlider.get());

    IIButton.reset (new juce::ToggleButton());
    IIButton->setTooltip ("if on the input gets mobulated (multiplied) with the bubbles");
    IIButton->setButtonText (valueTreeState.getParameter (NAME_II)->getName (20));
    addAndMakeVisible (IIButton.get());

    BNAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_BN, *BNSlider));
    MRAttachment.reset (new                             TwoValueSliderAttachment(valueTreeState, NAME_MIR,NAME_MAR, *MRSlider));
    ERAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_ER, *ERSlider));
    MDAttachment.reset (new                             TwoValueSliderAttachment(valueTreeState, NAME_MID,NAME_MAD, *MDSlider));
    EDAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_ED, *EDSlider));
    RFAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_RF, *RFSlider));
    RCAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_RC, *RCSlider));
    ARAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_AR, *ARSlider));
    GAAttachment.reset (new juce::AudioProcessorValueTreeState::SliderAttachment(valueTreeState, NAME_GA, *GASlider));
    IIAttachment.reset (new juce::AudioProcessorValueTreeState::ButtonAttachment(valueTreeState, NAME_II, *IIButton));

    setSize (600, 400);
    setLookAndFeel (&mylaf);
}

PluginEditor::~PluginEditor()
{
    BNAttachment.reset();
    MRAttachment.reset();
    ERAttachment.reset();
    MDAttachment.reset();
    EDAttachment.reset();
    RFAttachment.reset();
    RCAttachment.reset();
    ARAttachment.reset();
    GAAttachment.reset();
    IIAttachment.reset();

    this->setLookAndFeel(nullptr);

    BNSlider = nullptr;
    MRSlider = nullptr;
    ERSlider = nullptr;
    MDSlider = nullptr;
    EDSlider = nullptr;
    RFSlider = nullptr;
    RCSlider = nullptr;
    ARSlider = nullptr;
    GASlider = nullptr;
    IIButton = nullptr;
}

//==============================================================================
void PluginEditor::paint (juce::Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

void PluginEditor::resized()
{
    auto area = getLocalBounds ().reduced (20);
    area.removeFromLeft (area.getWidth ()/5); //for labels
    auto component_heignt = getHeight ()/N_CONTROLS;
//    BNSlider->setBounds (area.removeFromTop (Component_height));
    IIButton->setBounds (area.removeFromTop (component_heignt));
    MRSlider->setBounds (area.removeFromTop (component_heignt));
    ERSlider->setBounds (area.removeFromTop (component_heignt));
    MDSlider->setBounds (area.removeFromTop (component_heignt));
    EDSlider->setBounds (area.removeFromTop (component_heignt));
    RFSlider->setBounds (area.removeFromTop (component_heignt));
    RCSlider->setBounds (area.removeFromTop (component_heignt));
    ARSlider->setBounds (area.removeFromTop (component_heignt));
    GASlider->setBounds (area.removeFromTop (component_heignt));
//    MRLabel.setBounds (MRLabel.getBounds ());
//    MDLabel.setBounds (MDLabel.getBounds ());
}
