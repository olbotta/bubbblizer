#pragma once
#include <JuceHeader.h>

//in questo caso i parametri li teniamo qua perchè non devono essere utilizzati in varie classi, come quelli dei DSP

class MyLookAndFeel : public LookAndFeel_V4
{
public:
	MyLookAndFeel()
    {
        setColour (ResizableWindow::backgroundColourId,Colour::fromRGB (166, 227, 234));
        setColour (Slider::thumbColourId,Colour::fromRGB (234, 165, 227));
        setColour (ToggleButton::tickColourId,Colour::fromRGB (234, 165, 227));
        setColour (ToggleButton::tickDisabledColourId,Colour::fromRGB (234, 165, 227));
        setColour (ToggleButton::textColourId,Colours::black);
        setColour (Slider::textBoxTextColourId,Colours::black);
        setColour (Label::textColourId,Colours::black);
	}

    void drawLinearSlider (Graphics& g, int x, int y, int width, int height,
                                           float sliderPos,
                                           float minSliderPos,
                                           float maxSliderPos,
                                           const Slider::SliderStyle style, Slider& slider) override
    {
        if (slider.isBar())
        {
            g.setColour (slider.findColour (Slider::trackColourId));
            g.fillRect (slider.isHorizontal() ? Rectangle<float> (static_cast<float> (x), (float) y + 0.5f, sliderPos - (float) x, (float) height - 1.0f)
                                              : Rectangle<float> ((float) x + 0.5f, sliderPos, (float) width - 1.0f, (float) y + ((float) height - sliderPos)));
        }
        else
        {
            auto isTwoVal   = (style == Slider::SliderStyle::TwoValueVertical   || style == Slider::SliderStyle::TwoValueHorizontal);
            auto isThreeVal = (style == Slider::SliderStyle::ThreeValueVertical || style == Slider::SliderStyle::ThreeValueHorizontal);

            auto trackWidth = jmin (25.0f, slider.isHorizontal() ? (float) height * 0.5f : (float) width * 0.5f);

            Point<float> startPoint (slider.isHorizontal() ? (float) x : (float) x + (float) width * 0.5f,
                                     slider.isHorizontal() ? (float) y + (float) height * 0.5f : (float) (height + y));

            Point<float> endPoint (slider.isHorizontal() ? (float) (width + x) : startPoint.x,
                                   slider.isHorizontal() ? startPoint.y : (float) y);

            Path backgroundTrack;
            backgroundTrack.startNewSubPath (startPoint);
            backgroundTrack.lineTo (endPoint);
            g.setColour  (slider.findColour (Slider::backgroundColourId));
            g.setGradientFill (ColourGradient(slider.findColour (Slider::backgroundColourId),startPoint,(Colours::aliceblue).darker (1.7f),endPoint,false));
            g.strokePath (backgroundTrack, { trackWidth, PathStrokeType::curved, PathStrokeType::rounded });

            Path valueTrack;
            Point<float> minPoint, maxPoint, thumbPoint;

            if (isTwoVal || isThreeVal)
            {
                minPoint = { slider.isHorizontal() ? minSliderPos : (float) width * 0.5f,
                             slider.isHorizontal() ? (float) height * 0.5f : minSliderPos };

                if (isThreeVal)
                    thumbPoint = { slider.isHorizontal() ? sliderPos : (float) width * 0.5f,
                                   slider.isHorizontal() ? (float) height * 0.5f : sliderPos };

                maxPoint = { slider.isHorizontal() ? maxSliderPos : (float) width * 0.5f,
                             slider.isHorizontal() ? (float) height * 0.5f : maxSliderPos };
            }
            else
            {
                auto kx = slider.isHorizontal() ? sliderPos : ((float) x + (float) width * 0.5f);
                auto ky = slider.isHorizontal() ? ((float) y + (float) height * 0.5f) : sliderPos;

                minPoint = startPoint;
                maxPoint = { kx, ky };
            }

            auto thumbWidth = getSliderThumbRadius (slider);

            valueTrack.startNewSubPath (minPoint);
            valueTrack.lineTo (isThreeVal ? thumbPoint : maxPoint);
            g.setColour (slider.findColour (Slider::trackColourId));
            g.strokePath (valueTrack, { thumbWidth, PathStrokeType::curved, PathStrokeType::rounded });

            if (! isTwoVal)
            {
                g.setColour (slider.findColour (Slider::thumbColourId));

                g.drawEllipse (Rectangle<float> (static_cast<float> (thumbWidth), static_cast<float> (thumbWidth)).withCentre (isThreeVal ? thumbPoint : maxPoint),thumbWidth/5);

            }

            if (isTwoVal || isThreeVal)
            {
                auto sr = jmin (trackWidth, (slider.isHorizontal() ? (float) height : (float) width) * 0.4f);
                auto pointerColour = slider.findColour (Slider::thumbColourId);

                if (slider.isHorizontal())
                {
                    drawPointer (g, minSliderPos,
                                 jmax (0.0f, (float) y + (float) height * 0.5f),
                                 trackWidth, pointerColour, 2);

                    drawPointer (g, maxSliderPos,
                                 jmin ((float) (y + height) * 1.0f, (float) y + (float) height * 0.5f),
                                 trackWidth, pointerColour, 4);
                }
                else
                {
                    drawPointer (g, jmax (0.0f, (float) x + (float) width * 0.5f),
                                 minSliderPos - trackWidth,
                                 trackWidth, pointerColour, 1);

                    drawPointer (g, jmin ((float) (x + width), (float) x + (float) width * 0.5f), maxSliderPos,
                                 trackWidth, pointerColour, 3);
                }
            }
        }
    }

    int getSliderThumbRadius (Slider& slider) override
    {
        return jmin (25, slider.isHorizontal() ? static_cast<int> ((float) slider.getHeight() * 0.5f)
                                               : static_cast<int> ((float) slider.getWidth()  * 0.5f));
    }

    void drawPointer (Graphics& g, const float x, const float y, const float diameter,
                                      const Colour& colour, const int direction) noexcept
    {
        g.setColour (colour);
        g.drawEllipse (Rectangle<float> (static_cast<float> (diameter), static_cast<float> (diameter)).withCentre (Point<float>(x,y)),diameter/5);
    }

    void drawTickBox (Graphics& g, Component& component,
                                      float x, float y, float w, float h,
                                      const bool ticked,
                                      const bool isEnabled,
                                      const bool shouldDrawButtonAsHighlighted,
                                      const bool shouldDrawButtonAsDown)
    {
        ignoreUnused (isEnabled, shouldDrawButtonAsHighlighted, shouldDrawButtonAsDown);

        Rectangle<float> tickBounds (x, y, w, h);

        const auto cornerSize    = 3.0f;
        const auto lineThickness = 3.0f;

        g.setColour (component.findColour (ToggleButton::tickDisabledColourId));
        g.drawRoundedRectangle (tickBounds, cornerSize, lineThickness);

        if (ticked)
        {
            g.setColour (component.findColour (ToggleButton::tickColourId));
            g.fillRoundedRectangle (tickBounds.reduced (lineThickness),cornerSize);
        }
    }
private:

};
